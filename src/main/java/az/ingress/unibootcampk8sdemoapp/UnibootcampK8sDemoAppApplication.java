package az.ingress.unibootcampk8sdemoapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnibootcampK8sDemoAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnibootcampK8sDemoAppApplication.class, args);
    }

}
